#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define MAX_DIGITS 50
unsigned long long get_time()
{
    struct timespec ts;
    timespec_get(&ts, TIME_UTC);
    unsigned long long ret = ts.tv_sec * 1000000000 + ts.tv_nsec;
    //printf("Time: %llu \n", ret);
    char *str_ret = malloc (sizeof(char) * MAX_DIGITS);
    //sprintf (str_ret, "%llu", ret);
    //printf("Time S : %s \n", str_ret);
    return ret;	
}
