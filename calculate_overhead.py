import argparse
import numpy as np
import pandas as pd

parser = argparse.ArgumentParser(description="Calculatign Baseline for experiments", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-baseline", "--baseline",    type=str, help="Path to baseline directory")
parser.add_argument("-experiment","--experiment", type=str, help="Path to experiment directory", default=0)
args = parser.parse_args()

baseline_dir = args.baseline
experiment_dir = args.experiment

def read_file_int(path):
    with open(path) as ff:
        lines = ff.readlines()
        val=int(lines[0])
    
    return val

def read_execution_times(experiment):
    with open("%s/%s.log" %(experiment, experiment)) as f:
        executions = f.readlines()
        
    execution_times={}
    for execution in executions:
        execution = execution.strip()
        execution_fields = execution.split(";")
        process_id = execution_fields[0]
        benchmark = execution_fields[1]
        execution_time=read_file_int("%s/execution_time_%s" %(experiment, process_id))
        execution_times[benchmark]=execution_time

    return execution_times


#Prepare Overhead Data

baseline_executions = read_execution_times(baseline_dir)
experiment_executions = read_execution_times(experiment_dir)

f = open("%s/overhead" %(experiment_dir), "a")
f.seek(0)
f.truncate()
f.write("benchmark, baseline, experiment wit probes, overhead \n")
for benchmark, baseline_time in baseline_executions.iteritems():
    experiment_time = experiment_executions[benchmark]
    overhead = (experiment_time - baseline_time) / (baseline_time + 0.0)
    f.write("%s,%d,%d,%f \n" %(benchmark, baseline_time, experiment_time, overhead))

f.truncate()
f.close()
