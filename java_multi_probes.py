#!/usr/bin/python
import argparse
from time import sleep
from sys import argv
import ctypes as ct
from bcc import BPF, USDT
import inspect
import os
import ctypes
from ctypes import cdll
from ctypes import c_ulonglong
import pickle
#lib = cdll.LoadLibrary('/home/kmahmou1/bcc_energy/time.so')
# find lib on linux or windows
#lib.get_time.restype = ctypes.c_ulonglong
parser = argparse.ArgumentParser(description="Tracing Different Java DTrace Probes", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-p", "--pid", type=int, help="The id of the process to trace.")
parser.add_argument("-probes", "--probes", default="monitor__wait",type=str, help="The name of the JVM probe.")
parser.set_defaults(verbose=False)
args = parser.parse_args()
this_pid = int(args.pid)
logs=open("logs_%d" %(this_pid), "a+")
probe_names = args.probes
event_notifications=[]
print("Probe Namses Is : %s " %(probe_names))
print("Trying to Call PAPI Function. Let's see how it goes")
probe_names = probe_names.split(",")
probes_times=[]


def shutdown(cput, data, size):
    logs.write("JVM Is Shutting Down! See you later \n")
    logs.write("Print JVM Probes to a File \n")
    
    logs.write("Number of Events %d \n" %(len(probes_times)))
    
    with open("%d_probes"%(this_pid), 'wb') as fp:  
        probe_index=0
        for inf in probes_times:
            fp.write("%s \n" %(inf))

    quit()

def notify_probe(probe_no,cpu, data, size):
    pname = probe_names[probe_no]
    event = bpf_ctx["%s" %(pname)].event(data)
    notification_info = "%s,%d" %(pname, event.ts)
    probes_times.append(notification_info)


def notify_probe_test(cpu, data, size):
    print("Oooh my Goooooooooood That's a looooooooooooooooooooooooot")
    pname = probe_names[probe_no]
    event = bpf_ctx["%s" %(pname)].event(data)
    probes_times[probe_no].append(event.ts)



debugLevel=0
if args.verbose:
    debugLevel=4


bpf_text = """
#include <uapi/linux/ptrace.h>

struct data_t {
    u32 pid;
    u64 ts;
    char comm[100];
};
           
BPF_PERF_OUTPUT(vm_shutdown);

//Event definitions for other probes
%s


//Function definitions for other probes
%s

int notify_shutdown(void *ctx) { 
     struct data_t data = {};
     data.pid = bpf_get_current_pid_tgid();
     data.ts = bpf_ktime_get_ns();
     bpf_get_current_comm(&data.comm, sizeof(data.comm));
     vm_shutdown.perf_submit(ctx, &data, sizeof(data));   
     return 0;
}

"""
probe_notification_function="""
int notify_%s(void *ctx) { 
     struct data_t data = {};
     data.pid = bpf_get_current_pid_tgid();
     data.ts = bpf_ktime_get_ns();
     bpf_get_current_comm(&data.comm, sizeof(data.comm));
     %s.perf_submit(ctx, &data, sizeof(data));
     return 0;
}
"""

def register_event(probe_index, pname):
    notify_lambda = lambda cpu, data, size: notify_probe(probe_index, cpu,data,size)
    logs.write("Registering Probe %s \n" %(pname))
    bpf_ctx["%s" %(pname)].open_perf_buffer(notify_lambda)


def generate_c_program(names):
    probe_definitions = map(lambda x: "BPF_PERF_OUTPUT(%s);" %(x), names)
    probe_definitions_code = "\n".join(probe_definitions)
    probe_functions = map(lambda x: probe_notification_function %(x,x),names)
    probe_functions_code = "\n".join(probe_functions)
    bpf_code = bpf_text %(probe_definitions_code, probe_functions_code)
    return bpf_code

# Create USDT context
logs.write("Attaching probes to pid %d \n" % this_pid)
code=generate_c_program(probe_names)
logs.write(code)
logs.write("\n")
usdt_ctx = USDT(pid=this_pid)
usdt_ctx.enable_probe(probe="vm__shutdown", fn_name="notify_shutdown")
logs.write("Probe Name %d \n"  %(len(probe_names)))
probes_times=[]

pindex=0
for pname in probe_names:
    logs.write("Enabling Probe %s \n" %(pname))
    usdt_ctx.enable_probe(probe=pname, fn_name="notify_%s" %(pname))


bpf_ctx = BPF(text=code, usdt_contexts=[usdt_ctx])
bpf_ctx["vm_shutdown"].open_perf_buffer(shutdown)

pindex=0
for pname in probe_names:
    register_event(pindex,pname)
    pindex=pindex+1

while 1:
        bpf_ctx.perf_buffer_poll()

    
