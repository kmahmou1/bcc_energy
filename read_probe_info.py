import sys

import os
import argparse
import numpy as np
import pandas as pd


probes_ds = pd.read_csv("report.csv", header = None, names=["no_intervals","no_probes","coefficient","probe_name","lib","bench"])

probes_ds=probes_ds.loc[pd.isna(probes_ds['coefficient'])==False]
probes_ds.to_csv("single_probes_info.csv");
probes_ds = probes_ds.groupby(['probe_name']).agg({"coefficient": "mean", "lib":"first","no_probes":"mean"})
probes_ds["abs_coefficient"] = abs(probes_ds["coefficient"])
probes_ds = probes_ds.sort_values(by=['abs_coefficient'], ascending=False)
probes_ds = probes_ds.drop(columns=['abs_coefficient'])
probes_ds.to_csv("sumamry_probes.csv")
