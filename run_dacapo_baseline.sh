export LD_PRELOAD=/usr/local/lib/libpapi.so.5
./run_baseline_benchmark.sh $2 $1
ppid=$(tail -1 java_ppid)
echo "Tracking Process $ppiid"
echo "$ppid-$1-$2" >> baseline_multiple_probes
tail --pid=${ppid} -f /dev/null
