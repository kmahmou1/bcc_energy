public class PerfUtils {

	static {
		
		//The library name should be lib_perf_utils.so
		System.loadLibrary("_perf_utils");
		
	}
	/*****************************************************************************/
	public static native void start_recording();
	public static native void stop_recording();
	public static native void register_thread_stat(); // Synchronized
	public static native void read_counters_reset();
	public static native void sysReadCountersReset();
	public static native void init_papi_lib();
	public static native void set_energy_enabled(int flag);	

	public static native void init_papi(int len, byte[][] counter_bytes);
	public static native void timer_init_events(int len,byte[][] counter_bytes);  

	public static native void init_event_names(int length, byte[][] counter_bytes, int THREAD_COUNTER_NAMES);
	/*****************************************************************************/


	public static void main(String[] args) {
		System.out.println("Just test calling the C functions");
		init_papi_lib();
	}

}
