input="$1/$1.log"
while IFS= read -r var
do
	bench=$(echo $var | cut -d ';' -f 2)
	pid=$(echo   $var | cut -d';' -f 1)
	probe_name=$(echo $var | cut -d';' -f 4)
	lib_name=$(echo $var | cut -d';' -f 5)
	echo "Processing bench $bench -- pid $pid -- probe_name $probe_name lib_name: $lib_name"	
	python coefficient.py -p ${pid} -probe ${probe_name} -lib ${lib_name} -bench $bench	
done < "$input"

echo "Calculating Coefficient Done ... Thanks for your cooperation"
