package research.utils.dacapo;

import java.util.Map;
import java.lang.reflect.Field;
import java.io.IOException;
import research.utils.perf.PerfUtils;
import org.dacapo.harness.Callback;
import org.dacapo.harness.CommandLineArgs;
import research.utils.instrumentation.profiling.ProfilingAgent;


import java.io.PrintWriter;

public class IterationCallBack extends Callback {


  int iter = 0;
  public static int WARM_UP_ITERS = 2;
  public static int TOTAL_ITERS = 10;
  public static String SCRIPT_CMD="sudo python /home/kmahmou1/bcc_energy/java_multi_probes.py -p %d -probes %s";
  public static String DTRACE_EVENTS=null;
  public static boolean READ_RAPL = false;  
  public static long start=0;
  public static final int MAX_ITERATIONS = 30;
  public static long[] ITERATION_TIMESTAMPS = new long[MAX_ITERATIONS];
  public static void vm_termination() { 
	  Runtime.getRuntime().addShutdownHook(
		new Thread() {
			public void run() {
				long end = System.currentTimeMillis();
				
				if(READ_RAPL) {
					ProfilingAgent.stop_recording();
				}
				
				long execution_time = end - start;
				long pid = ProcessHandle.current().pid();
				String execution_file_name = "execution_time_"+pid;
				write_to_file(execution_file_name,Long.toString(execution_time));

				StringBuffer iteration_times = new StringBuffer();
				for(int i=0; i < MAX_ITERATIONS;i++) {
					iteration_times.append(ITERATION_TIMESTAMPS[i]).append("\n");
				}

				write_to_file("iteration_time_"+pid, iteration_times.toString());
			}
			}
		);
  }

  public IterationCallBack(CommandLineArgs args) {
    super(args);
    vm_termination();
    try {
	WARM_UP_ITERS = Integer.parseInt(System.getenv("WARM_UP_ITERS"));
    } catch(Exception exception) {

    }

    try {
    	READ_RAPL = Boolean.parseBoolean(System.getenv("READ_RAPL"));
    } catch(Exception exception) {
		exception.printStackTrace();
    }

    try {
	TOTAL_ITERS = Integer.parseInt(System.getenv("TOTAL_ITERS"));
    } catch(Exception exception) {

    }


   System.out.println("Benchmark Parameters ........................");
   System.out.println(String.format("TOTAL_ITERS %d",   TOTAL_ITERS));
   System.out.println(String.format("READ_RAPL %s", READ_RAPL));
   start = System.currentTimeMillis();
   if(READ_RAPL) {
	   ProfilingAgent.start_recording();
   }
  }

  @Override
  public void start(String benchmark) {
    super.start(benchmark);
    ITERATION_TIMESTAMPS[iter]=PerfUtils.get_timestamp(); 
    iter++;
  }

  @Override
  public void complete(String benchmark, boolean valid) {
    super.complete(benchmark, valid);
  };


  public static void write_to_file(String file_name, String value) {
		try {
			PrintWriter writer = new PrintWriter(file_name);
			writer.println(value);
			writer.flush();
			writer.close();
		} catch(Exception exc) {
			exc.printStackTrace();
		}
   }
}
