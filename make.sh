rm research/utils/instrumentation/profiling/*.class
rm research/utils/dacapo/*.class
rm research/utils/perf/*.class


$JAVA_HOME/bin/javac -cp lib/javassist.jar:. research/utils/instrumentation/profiling/*.java
$JAVA_HOME/bin/javac -cp lib/javassist.jar:lib/dacapo-9.12-MR1-bach.jar:. research/utils/dacapo/*.java
$JAVA_HOME/bin/javac  research/utils/perf/*.java -h .
mv research_utils_perf_PerfUtils.h PerfUtils.h
export JAVA_HOME=/home/kmahmou1/openjdk-instrumentation/build/linux-x86_64-server-release/images/jdk
echo $JAVA_HOME

gcc -shared -o time.so time.c
gcc -lpthread -shared -fPIC -lpfm -o lib_perf_utils.so -I/usr/local/lib -I$JAVA_HOME/include/ -I$JAVA_HOME/include/linux  /usr/local/lib/libpapi.so papi_perf.c perf_utils.c perf_utils.h PerfUtils.c PerfUtils.h
