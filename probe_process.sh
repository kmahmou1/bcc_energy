export LD_PRELOAD=/usr/local/lib/libpapi.so.5
$1
experiment_dir=$3
pid=$(tail -1 process_id)
python java_multi_probes.py -p ${pid} -probes $2
ppid=$!

tail --pid=${!pid} -f /dev/null
tail --pid=${!ppid} -f /dev/null

mv "execution_time_${pid}" ${experiment_dir}

if [ -f "${pid}_probes" ]
then
	echo "mv ${pid}_probes ${experiment_dir}"
	mv ${pid}_probes ${experiment_dir}
fi

mv "iteration_time_$pid" ${experiment_dir}

if [ -f "energy_${pid}.data" ]
then
	mv "energy_${pid}.data" ${experiment_dir}
fi

if [ -f logs_$pid ]
then
	mv logs_$pid ${experiment_dir}
fi

