#!/usr/bin/python
import argparse
from time import sleep
from sys import argv
import ctypes as ct
from bcc import BPF, USDT
import inspect
import os

# Parse command line arguments
parser = argparse.ArgumentParser(description="Testing Java Probe Examples",
    formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-p", "--pid", type=int, help="The id of the process to trace.")
parser.add_argument("-f", "--filterstr", type=str, default="", help="The prefix filter for the operation input. If specified, only operations for which the input string starts with the filterstr are traced.")
parser.add_argument("-v", "--verbose", dest="verbose", action="store_true", help="If true, will output verbose logging information.")
parser.set_defaults(verbose=False)
args = parser.parse_args()
this_pid = int(args.pid)
this_filter = str(args.filterstr)


debugLevel=0
if args.verbose:
    debugLevel=4

# BPF program
bpf_text = """
#include <uapi/linux/ptrace.h>


struct data_t {
    u32 pid;
    u64 ts;
    char comm[100];
};
           
BPF_PERF_OUTPUT(ev);

int vm__shutdown(void *ctx) {
     struct data_t data = {};
     data.pid = bpf_get_current_pid_tgid();
     data.ts = bpf_ktime_get_ns();
     bpf_get_current_comm(&data.comm, sizeof(data.comm));
     ev.perf_submit(ctx, &data, sizeof(data));
     return 0;
}

"""

# Create USDT context
print("Attaching probes to pid %d" % this_pid)
usdt_ctx = USDT(pid=this_pid)
usdt_ctx.enable_probe(probe="vm__shutdown", fn_name="vm__shutdown")
# Create BPF context, load BPF program
bpf_ctx = BPF(text=bpf_text, usdt_contexts=[usdt_ctx], debug=debugLevel)

def print_event(cpu, data, size):
    event = bpf_ctx["ev"].event(data)
    print("Oh my Gooooooooooood")
    print(event.ts)
    quit()
    
bpf_ctx["ev"].open_perf_buffer(print_event)
while 1:
        bpf_ctx.perf_buffer_poll()

    
