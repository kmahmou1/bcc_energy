#!/usr/bin/python
import argparse
import subprocess

benchmarks_str="avrora.large,h2.large,jython.large,sunflow.large,tradebeans.large,tradesoap.large,xalan.large,lusearch-fix.default,luindex.default,pmd.default"
benchmarks=benchmarks_str.split(',')
parser = argparse.ArgumentParser(description="Probes Parser", formatter_class=argparse.RawDescriptionHelpFormatter)

parser.set_defaults(verbose=False)
args = parser.parse_args()

f = open("%s" %("launch_baseline.sh"), "w")
f.seek(0)
bindex=0


f.write("rm -rf $1 \n")
f.write("mkdir $1 \n")
f.write("\n")
for benchmark in benchmarks:
        benchmark_info = benchmark.split(".")
        bench = benchmark_info[0]
        size  = benchmark_info[1]
        cmd = "bash run_benchmark.sh -b %s -s %s -i %d -w %d -l \"%s\" -f $1 \n"
        log="%s;%s;%s;%s;%d;%d" %(bench,size,"No-Probes","NA",10,-1)
        command = cmd %(bench,size,10,-1,log)
        f.write(command)
        f.write("pid=$(head -1 process_id) \n")
        f.write("tail --pid=${!pid} -f /dev/null \n")
        f.write("\n")



f.write("mv $1.log $1/")
f.truncate()
f.flush()
f.close()

