#!/usr/bin/python
import argparse
from time import sleep
from sys import argv
import ctypes as ct
from bcc import BPF, USDT
import inspect
import os
import ctypes
from ctypes import cdll
from ctypes import c_ulonglong
import pickle
#lib = cdll.LoadLibrary('/home/kmahmou1/bcc_energy/time.so')
# find lib on linux or windows
#lib.get_time.restype = ctypes.c_ulonglong
parser = argparse.ArgumentParser(description="Tracing Different Java DTrace Probes", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-p", "--pid", type=int, help="The id of the process to trace.")
parser.add_argument("-probe", "--probe", default="monitor__wait",type=str, help="The name of the JVM probe.")
parser.set_defaults(verbose=False)
args = parser.parse_args()
this_pid = int(args.pid)
probe_name = args.probe
event_notifications=[]
print("Probe Name Is : %s " %(probe_name))
print("Trying to Call PAPI Function. Let's see how it goes")

debugLevel=0
if args.verbose:
    debugLevel=4

# BPF program
bpf_text = """
#include <uapi/linux/ptrace.h>

#define VM_SHUTDOWN 0
#define NORMAL_PROBE 1

struct data_t {
    u32 pid;
    u64 ts;
    char comm[100];
};
           
BPF_PERF_OUTPUT(events);
BPF_PERF_OUTPUT(vm_shutdown);

static void notify_userspace(int tp,void *ctx) { struct data_t data = {};
     data.pid = bpf_get_current_pid_tgid();
     data.ts = bpf_ktime_get_ns();
     bpf_get_current_comm(&data.comm, sizeof(data.comm));

     if(tp==VM_SHUTDOWN)
         vm_shutdown.perf_submit(ctx, &data, sizeof(data));
     else
         events.perf_submit(ctx, &data, sizeof(data));
     
}

int monitor_probe(void *ctx) {
     notify_userspace(NORMAL_PROBE, ctx);
     return 0;
}

int vm__shutdown(void *ctx) {
     notify_userspace(VM_SHUTDOWN, ctx);
     return 0;
}
"""

# Create USDT context
print("Attaching probes to pid %d" % this_pid)
usdt_ctx = USDT(pid=this_pid)
usdt_ctx.enable_probe(probe=probe_name, fn_name="monitor_probe")
usdt_ctx.enable_probe(probe="vm__shutdown", fn_name="vm__shutdown")
# Create BPF context, load BPF program
bpf_ctx = BPF(text=bpf_text, usdt_contexts=[usdt_ctx])

def print_event(cpu, data, size):
    event = bpf_ctx["events"].event(data)
    event_notifications.append(event.ts)


def shutdown(cput, data, size):
    print("JVM Is Shutting Down! See you later")
    print("Print JVM Probes to a File")

    with open("%d_probes"%(this_pid), 'wb') as fp:
        for t in event_notifications:
                    fp.write("%d\n" % t)

    quit()

bpf_ctx["events"].open_perf_buffer(print_event)
bpf_ctx["vm_shutdown"].open_perf_buffer(shutdown)

while 1:
        bpf_ctx.perf_buffer_poll()

    
