export LD_PRELOAD=/usr/local/lib/libpapi.so.5
core_counters=""
package_counters="PAPI_L3_TCM"
ppid=""
echo "Executing Benhmark With Instrumentation $1 ~~~ $2"
java -Dexec_file="$2_$1_perf.time" -Djava.library.path=.  -classpath . -javaagent:profiling.jar -jar dacapo-9.12-bach.jar -s $1 $2 -no-validation &
ppid=$!
echo "$ppid" > java_ppid
