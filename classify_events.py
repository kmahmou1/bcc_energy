#!/usr/bin/python
import argparse
import subprocess
parser = argparse.ArgumentParser(description="Probes Parser", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-file", "--file", type=str, help="The path to the bcc list output file that contains probes")
parser.set_defaults(verbose=False)
args = parser.parse_args()
file_path = args.file
content=[]
benchmarks=["sunflow","lusearch","xalan ","avrora","jython","luindex","h2"]
sizes=["large","large","default","large","small","small","small"]
with open(file_path) as f:
        content = f.readlines()

f = open("launch.sh", "w")
f.seek(0)
for line in content:
    line=line.strip()
    line_parts=line.partition(":")
    lib_info=line_parts[0]
    probe_name=line_parts[2]
    bindex=0
    for bench in benchmarks:
        size=sizes[bindex]
        bindex=bindex+1
        cmd = "bash run_dacapo_with_probe_monitor.sh %s \"%s\" %s %s" %(probe_name,lib_info, bench, size)
        f.write(cmd)
        f.write("\n")

f.truncate()
f.flush()
f.close()

