baseline="baseline_multiple_probes"
multiple_probes="multiple_probes"
echo "benchmark, baseline execution time, probes execution time, overhead"
while IFS= read -r baseline_row && IFS= read -r multiple_probes_row <&3; do
	baseline_id=$(echo "$baseline_row "|awk 'BEGIN { FS = "-" } ; { print $1 }')
	withprobes_id=$(echo "$multiple_probes_row "|awk 'BEGIN { FS = "-" } ; { print $1 }')
	bench_name=$(echo "$multiple_probes_row "|awk 'BEGIN { FS = "-" } ; { print $2 }')
	baseline_execution=$(head -1 "execution_time_$baseline_id")
	probes_execution=$(head -1 "execution_time_$withprobes_id")
	overhead=$(awk -v b=$baseline_execution -v p=$probes_execution 'BEGIN { print (p-b)/b}')
	echo "$bench_name, $baseline_execution, $probes_execution,$overhead"
done <$baseline 3< $multiple_probes
