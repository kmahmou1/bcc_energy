bash run_benchmark.sh -b avrora -s small -e mem__pool__gc__end -i 10 -w 5 -l "avrora;large;mem__pool__gc__end;hotspot;10;5" -f single_probes -p
bash run_benchmark.sh -b sunflow -s large -e mem__pool__gc__begin -i 10 -w 5 -l "avrora;large;mem__pool__gc__begin;hotspot;10;5" -f single_probes -p
bash run_benchmark.sh -b h2 -s small -e class__unloaded -i 10 -w 5 -l "avrora;large;class__unloaded;hotspot;10;5" -f single_probes -p
