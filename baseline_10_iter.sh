rm -rf $1
bash run_benchmark.sh -b sunflow -s large -i 10 -w 5 -l "sunflow;large;No-Probes;NA;10;-1" -f $1
bash run_benchmark.sh -b avrora -s large -i 10 -w 5 -l "avrora;large;No-Probes;NA;10;-1" -f $1
bash run_benchmark.sh -b h2 -s large -i 10 -w 5 -l "h2;large;No-Probes;NA;10;-1" -f $1
bash run_benchmark.sh -b jython -s large -i 10 -w 5 -l "jython;large;No-Probes;NA;10;-1" -f $1
bash run_benchmark.sh -b tradebeans -s large -i 10 -w 5 -l "tradebeans;large;No-Probes;NA;10;-1" -f $1
bash run_benchmark.sh -b tradesoap -s large -i 10 -w 5 -l "tradesoap;large;No-Probes;NA;10;-1" -f $1
bash run_benchmark.sh -b xalan -s large -i 10 -w 5 -l "xalan;large;No-Probes;NA;10;-1" -f $1
bash run_benchmark.sh -b lusearch-fix -s default -i 10 -w 5 -l "lusearch-fix;default;No-Probes;NA;10;-1" -f $1
bash run_benchmark.sh -b luindex -s default -i 10 -w 5 -l "luindex;default;No-Probes;NA;10;-1" -f $1
bash run_benchmark.sh -b pmd -s default -i 10 -w 5 -l "pmd;default;No-Probes;NA;10;-1" -f $1
