#!/usr/bin/python
import argparse
import subprocess

benchmarks_str="avrora.large,h2.large,jython.large,sunflow.large,tradebeans.large,tradesoap.large,xalan.large,lusearch-fix.default,luindex.default,pmd.default"
benchmarks=benchmarks_str.split(',')
parser = argparse.ArgumentParser(description="Probes Parser", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-file", "--file", type=str, help="The path to the bcc list output file that contains probes")
parser.set_defaults(verbose=False)
args = parser.parse_args()
file_path = args.file
probes_info=[]
line_indx=0

with open(file_path) as fp:  
    line = fp.readline()
    while(line):
        if(line_indx > 0):
            probes_info.append(line.strip())
        
        line_indx=line_indx+1
        line = fp.readline()

f = open("launch_single_probes.sh", "w")
f.seek(0)
bindex=0

f.write("rm -rf $1")
f.write("\n")
for benchmark in benchmarks:
        benchmark_info = benchmark.split(".")
        bench = benchmark_info[0]
        size  = benchmark_info[1]
        cmd = "bash run_benchmark.sh -b %s -s %s -e %s -i %d -w %d -l \"%s\" -f $1 -p"
        for probe_info in probes_info:
            probe_fields=probe_info.split(",")
            probe_name=probe_fields[0]
            probe_lib=probe_fields[3]
            log="%s;%s;%s;%s;%d;%d" %(bench,size,probe_name,probe_lib,2,1)
            command = cmd %(bench,size,probe_name,2,1,log)
            f.write(command)
            f.write("\n")

f.truncate()
f.flush()
f.close()

