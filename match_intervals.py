import argparse
import numpy as np
import pandas as pd
import sys

parser = argparse.ArgumentParser(description="Tracing Different Java DTrace Probes", formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument("-p", "--pid", type=int, help="The PID of the traced process")
parser.add_argument("-i","--i", type=int,help="The number of iterations", default=0)
parser.add_argument("-bench","--bench", type=str,help="Benchmark Name", default=0)
parser.add_argument("-experiment","--experiment", type=str,help="Experiment Folder", default=0)

args = parser.parse_args()
this_id = int(args.pid)
iterations = args.i
experiment = args.experiment
unique={}
probe_f = "%s/%d_probes" %(experiment,this_id)
energy_f = "%s/energy_%d.data" %(experiment,this_id)
iterations_f = "%s/iteration_time_%d" %(experiment, this_id)
energy_sum=[]
bench=args.bench

with open(iterations_f) as f:
    iteration_times = f.readlines()


iteration_time=int(iteration_times[iterations-1])

total_sum=0
energy_data = pd.read_csv(energy_f,names=["cconstant","energy_timestamp","counter_name","counter","pkg","dram","end"])
energy_data = energy_data.drop(['cconstant','counter_name','end'],axis=1)
probe_data = pd.read_csv(probe_f,names=["probe_name","probe_timestamp"])
energy_data_cpy = energy_data.diff().fillna(0)
energy_data_cpy = energy_data_cpy.shift(-1)
energy_data["pkg"]=energy_data_cpy["pkg"]
energy_data["dram"]=energy_data_cpy["dram"]
energy_data["energy_timestamp"][-1:]=sys.maxint


interval_no=energy_data.shape[0]
probe_no=probe_data.shape[0]

no_steps=0
probe_count=[]
for i in range(energy_data.shape[0]):
    probe_count.append(dict())

found=0
interval_distribution=0
interval_no = energy_data.shape[0];
found_intervals=0
print(probe_data.dtypes)
probe_data.sort_values(by=['probe_timestamp'],inplace=True)
energy_data.sort_values(by=['energy_timestamp'],inplace=True)
interval_indx=0
probe_data = probe_data.reset_index()
i=0


for i,probe_ts in probe_data["probe_timestamp"].items():
    probe_name = probe_data["probe_name"][i]
    if not(probe_name in unique):
        unique[probe_name]=0

    unique[probe_name]=unique[probe_name]+1
    while (interval_indx < interval_no-1):

        start = energy_data["energy_timestamp"][interval_indx]
        end = energy_data["energy_timestamp"][interval_indx+1]
        if(probe_ts < start and interval_indx==0):
            break
        
        if(probe_ts  > start and probe_ts <= end):
            mp = probe_count[interval_indx]
            if not(probe_name in  mp):
                mp[probe_name]=0
                
            mp[probe_name]=mp[probe_name]+1
            no_steps = no_steps+1
            found_intervals=found_intervals+1
            break
        else:  
            total_sum = sum(probe_count[interval_indx].values())
            energy_sum.append(total_sum)
            no_steps=no_steps+1
            interval_distribution=interval_distribution+1
            interval_indx=interval_indx+1


    if(interval_indx==interval_no):
        probe_count[interval_no-1]=probe_count[interval_no-1]
        no_steps=no_steps+1
        found_intervals=found_intervals+1



coff=energy_data["pkg"].corr(pd.Series(energy_sum))
summary_header="Process ID,#Steps ,# Probes, #Total Probes, #Intervals, #Found Intervales, Correlation"
summary="%d,%d,%d,%d,%d,%d,%f" %(this_id,no_steps,len(unique),probe_no,interval_no,found_intervals,coff)
final_counts=[]
for key,val in unique.items():
   pcount = "%s,%d" %(key,val)
   final_counts.append(pcount)



data_out=[]
for i,mp in enumerate(probe_count):
    energy_value = energy_data["pkg"][i]
    probes_str = str(energy_value) + ","
    probe_counts = probe_count[i]
    for pname in unique:
        if(pname in probe_counts):
            probes_str = probes_str + str(probe_counts[pname])  + ","
        else:
            probes_str = probes_str + "0,"

    probes_str = probes_str + "end"            
    data_out.append(probes_str)


summary_f = "%s/%s_summary" %(experiment,bench)
f = open(summary_f , "a")
f.seek(0)
f.truncate()
f.write(summary)
f.write("\n")


for fcount in final_counts:
    f.write(fcount)
    f.write("\n")

f.write("\n")
f.flush()
f.close()


f = open("%s/%s.regression" %(experiment,bench), "a")
f.seek(0)
f.truncate()
f.write("energy,")
for u in unique:
    f.write("%s," %(u))
f.write("end \n")
for line in data_out:
    f.write("%s \n" %(line))
f.close()
print("\n Total Sum %d \n" %(total_sum))
