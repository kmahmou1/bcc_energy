export LD_PRELOAD=/usr/local/lib/libpapi.so.5
./run_benchmark.sh 1 $4 $3
ppid=$(tail -1 java_ppid)
echo "Tracking Process $ppiid"
python java_multi_probes.py -p ${ppid} -probes $1
echo "$ppid-$3_$4-$1-$2" >> multiple_probes
