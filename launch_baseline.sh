rm -rf $1 
mkdir $1 

bash run_benchmark.sh -b avrora -s large -i 10 -w -1 -l "avrora;large;No-Probes;NA;10;-1" -f $1 
pid=$(head -1 process_id) 
tail --pid=${!pid} -f /dev/null 

bash run_benchmark.sh -b h2 -s large -i 10 -w -1 -l "h2;large;No-Probes;NA;10;-1" -f $1 
pid=$(head -1 process_id) 
tail --pid=${!pid} -f /dev/null 

bash run_benchmark.sh -b jython -s large -i 10 -w -1 -l "jython;large;No-Probes;NA;10;-1" -f $1 
pid=$(head -1 process_id) 
tail --pid=${!pid} -f /dev/null 

bash run_benchmark.sh -b sunflow -s large -i 10 -w -1 -l "sunflow;large;No-Probes;NA;10;-1" -f $1 
pid=$(head -1 process_id) 
tail --pid=${!pid} -f /dev/null 

bash run_benchmark.sh -b tradebeans -s large -i 10 -w -1 -l "tradebeans;large;No-Probes;NA;10;-1" -f $1 
pid=$(head -1 process_id) 
tail --pid=${!pid} -f /dev/null 

bash run_benchmark.sh -b tradesoap -s large -i 10 -w -1 -l "tradesoap;large;No-Probes;NA;10;-1" -f $1 
pid=$(head -1 process_id) 
tail --pid=${!pid} -f /dev/null 

bash run_benchmark.sh -b xalan -s large -i 10 -w -1 -l "xalan;large;No-Probes;NA;10;-1" -f $1 
pid=$(head -1 process_id) 
tail --pid=${!pid} -f /dev/null 

bash run_benchmark.sh -b lusearch-fix -s default -i 10 -w -1 -l "lusearch-fix;default;No-Probes;NA;10;-1" -f $1 
pid=$(head -1 process_id) 
tail --pid=${!pid} -f /dev/null 

bash run_benchmark.sh -b luindex -s default -i 10 -w -1 -l "luindex;default;No-Probes;NA;10;-1" -f $1 
pid=$(head -1 process_id) 
tail --pid=${!pid} -f /dev/null 

bash run_benchmark.sh -b pmd -s default -i 10 -w -1 -l "pmd;default;No-Probes;NA;10;-1" -f $1 
pid=$(head -1 process_id) 
tail --pid=${!pid} -f /dev/null 

mv $1.log $1/
