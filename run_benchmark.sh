export LD_PRELOAD=/usr/local/lib/libpapi.so.5
core_counters=""
package_counters="PAPI_L3_TCM"

#core_counters="cycles,instructions"
#package_counters="PERF_COUNT_HW_CACHE_MISSES"
sys_exit=""
ppid=""
extended_probes=-XX:+ExtendedDTraceProbes 
extended_probes=""
print_assembly="-XX:+UnlockDiagnosticVMOptions -XX:CompileCommand=print,*.*"
dacapo_jar="lib/dacapo-9.12-MR1-bach.jar"
jars="/home/kmahmou1/bcc_energy/lib/javassist.jar:/home/kmahmou1/bcc_energy/lib/profiling.jar:$dacapo_jar"

benchmark=""
size=""
iterations="1"
callback="research.utils.dacapo.IterationCallBack"
script_log="No Log"
log_file="probe_logs"

while getopts 'l:w:b:s:i:pe:f:' c
do
	case $c in
		b) benchmark=$OPTARG;;
		s) size=$OPTARG;;
		i) iterations=$OPTARG;;
		p) export READ_RAPL=true;;
		e) export DTRACE_EVENTS=$OPTARG;extended_probes=-XX:+ExtendedDTraceProbes;;
		w) export WARM_UP_ITERS=$OPTARG;;
		l) script_log=$OPTARG;;
		f) log_file=$OPTARG;;
	esac
done

echo "Shell-READ_RAPL:$READ_RAPL"

experiment_dir=${log_file}
log_file="${log_file}.log"
echo "Processing $benchmark with sie $size done:"
echo "Executing Benchmark with no instrumentation : $benchmark ~~~ $size" 
echo $extended_probes
nnjava  $extended_probes -Djava.library.path=. -Xbootclasspath/a:javassist.jar -cp $jars Harness $benchmark -s $size -no-validation --iterations $iterations  --callback $callback &

pid=$!
echo "Java ID $pid"
echo $pid > process_id
echo "Processing $benchmark with sie $size done:"
echo "$pid;$script_log" >> $log_file
