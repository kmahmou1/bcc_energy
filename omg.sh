export LD_PRELOAD=/usr/local/lib/libpapi.so.5
core_counters=""
package_counters="PAPI_L3_TCM"

#core_counters="cycles,instructions"
#package_counters="PERF_COUNT_HW_CACHE_MISSES"
sys_exit=""
ppid=""
extended_probes=-XX:+ExtendedDTraceProbes 
extended_probes=""
print_assembly="-XX:+UnlockDiagnosticVMOptions -XX:CompileCommand=print,*.*"
jarname="lib/dacapo-9.12-MR1-bach.jar"

profile="0"
benchmark=""
size=""
iterations="1"
callback="research.utils.dacapo.IterationCallBack"

while getopts 'b:s:i:p' c
do
	case $c in
		b) benchmark=$OPTARG;;
		s) size=$OPTARG;;
		i) iterations=$OPTARG;;
		p) profile="1";;
	esac
done



if [ $profile -eq "0" ]
then
	echo "Executing Benchmark with no instrumentation : $3 ~~~ $2"
	nnjava  $extended_probes -Dexec_file="${benchmark}_${size}.time" -Xbootclasspath/a:javassist.jar:method_stats.jar -classpath . -javaagent:method_stats.jar="overhead_only"  -jar $jarname -s $size $benchmark -no-validation --iterations $iterations &

else
	echo "Executing Benhmark With Instrumentation $benchmark ~~~ $size"
	nnjava $extended_probes -Dexec_file="$2_$3_perf.time" -Djava.library.path=.  -classpath . -javaagent:profiling.jar -jar $jarname -s $size $benchmark -no-validation  --iterations $iterations --callback $callback &
	ppid=$!
fi

echo "$ppid" > java_ppid
