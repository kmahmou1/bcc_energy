import argparse
import numpy as np
import pandas as pd

parser = argparse.ArgumentParser(description="Tracing Different Java DTrace Probes", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-p", "--pid", type=int, help="The PID of the traced process")
parser.add_argument("-epochs","--epochs", type=int,help="The number of epochs", default=0)
parser.add_argument("-probe","--probe",type=str,default="None")
parser.add_argument("-lib","--lib",type=str,default="None")
parser.add_argument("-bench","--bench",type=str,default="None")

args = parser.parse_args()
this_id = int(args.pid)
epochs = args.epochs
bench = args.bench
probe_f = "%d_probes" %(this_id)
energy_f = "energy_%d.data" %(this_id)
probe_name=args.probe
lib_name=args.lib


print("Process ID %d" %(this_id))
print("Reading %s and %s ..." %(probe_f,energy_f))
print("Number of epochs %d" %(epochs))

energy_data = pd.read_csv(energy_f,names=["cconstant","energy_timestamp","counter_name","counter","pkg","dram","end"])
energy_data = energy_data.drop(['cconstant','counter_name','end'],axis=1)
#probe_data = pd.read_csv(probe_f,names=["probe_timestamp"])
f = open(probe_f, 'r')
probe_data = f.readlines()
probe_data = probe_data[1:]
energy_data_cpy = energy_data.diff().fillna(0)
energy_data_cpy = energy_data_cpy.shift(-1)
energy_data["pkg"]=energy_data_cpy["pkg"]
energy_data["dram"]=energy_data_cpy["dram"]


print("Total Number of Intervals %d" %(energy_data.shape[0]))
print("Total Number of Probes %d" %(len(probe_data)))
no_steps=0
probe_count=[0]*energy_data.shape[0]
last_interval_indx=0
found=0
interval_distribution=0
interval_no = energy_data.shape[0] - 1;
found_intervals=0
for probe_ts in probe_data:
    probe_ts = probe_ts.strip()
    probe_ts = int(probe_ts)
    interval_indx = last_interval_indx
    while (interval_indx < interval_no-1):
        if(probe_ts >= energy_data["energy_timestamp"][interval_indx]  and probe_ts < energy_data["energy_timestamp"][interval_indx+1]):
            probe_count[interval_indx] = probe_count[interval_indx]+1
            last_interval_indx=interval_indx
            no_steps = no_steps+1
            found_intervals=found_intervals+1
            break
        else:  
            no_steps=no_steps+1
            interval_indx = interval_indx+1
            interval_distribution=interval_distribution+1

    if(interval_indx==interval_no):
        probe_count[interval_no-1]=probe_count[interval_no-1]
        no_steps=no_steps+1
        found_intervals=found_intervals+1


energy_data["probe_count"]=probe_count
#print(energy_data[energy_data["probe_count"]>0])
#energy_data.to_csv("energy_data")

coff = energy_data['probe_count'].corr(energy_data['pkg'])
f = open("summary_%d" %(this_id), "a")
summary_str="# of probes,# of intervals,coefficient, probe name, library(so)"
f.write(summary_str)
f.write("\n")
summ="%d,%d,%f,%s,%s,%s" %(energy_data.shape[0],len(probe_data),coff,probe_name,lib_name,bench)
f.write(summ)


