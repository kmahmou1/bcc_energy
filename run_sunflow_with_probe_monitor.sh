export LD_PRELOAD=/usr/local/lib/libpapi.so.5
sudo ./run_benchmark_oldway.sh  -b sunflow -s large -i 10 -w -1
echo "Tracking Process $ppid"
ppid=$(tail -1 java_ppid)
python java_probe.py -p ${ppid} -probe $1
echo "$ppid-$1-$2" >> process_probe_info
