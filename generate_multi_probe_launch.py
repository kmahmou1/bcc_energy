#!/usr/bin/python
import argparse
import subprocess

benchmarks_str="sunflow.large,avrora.large,h2.large,jython.large,tradebeans.large,tradesoap.large,xalan.large,lusearch-fix.default,luindex.default,pmd.default"
benchmarks=benchmarks_str.split(',')


parser = argparse.ArgumentParser(description="Probes Parser", formatter_class=argparse.RawDescriptionHelpFormatter)


parser.add_argument("-in", "--input",type=str, help="File name that contains probe name to include")
parser.add_argument("-out", "--out"  ,type=str, help="Shell Script File Name")


parser.set_defaults(verbose=False)
args = parser.parse_args()
input_file = args.input
output_file = args.out

f = open("%s" %(output_file), "w")
f.seek(0)
bindex=0

data=""
with open(input_file, 'r') as file:
        data = file.read()


print("Probes %s" %(data))
print("Output file name %s" %(output_file))
#probes="mem__pool__gc__end,mem__pool__gc__begin,class__unloaded,gc__end,gc__begin,lll_lock_wait,mutex_destroy,memory_mallopt_free_dyn_thresholds,memory_heap_more,memory_heap_new"

#The following contains handpicked probes chosen by Professor David Lui based on Frequency of Probes and Correlation Coefficient. 
#probes="lll_lock_wait,cond_wait,cond_signal,mutex_release,mutex_entry,cond_init,mutex_init,GetPrimitiveArrayCritical__entry,class__initialization__required,GetPrimitiveArrayCritical__return,class__initialization__recursive,ReleasePrimitiveArrayCritical,cond_broadcast,GetObjectField__return,DeleteLocalRef__entry,DeleteLocalRef__return"

probes="memory_mallopt_free_dyn_thresholds,mem__pool__gc__begin,memory_heap_new,gc__begin,class__unloaded,lll_lock_wait,mutex_destroy"


probes=data.strip()
f.write("rm -rf $1 \n")
f.write("rm $1.log \n")
f.write("mkdir $1 \n")
f.write("\n")
for benchmark in benchmarks:
        benchmark_info = benchmark.split(".")
        bench = benchmark_info[0]
        size  = benchmark_info[1]
        cmd = "bash probe_process.sh \"bash run_benchmark.sh -b %s -s %s -i %d -l %s -f $1 -p\" %s $1 \n"
        log="%s;%s;%s;%s;%d" %(bench,size,probes,"NA",10)
        command = cmd %(bench,size,10,log,probes)
        f.write(command.strip())
        f.write("\nppid=$! \n")
        f.write("tail --pid=${!ppid} -f /dev/null \n")
        f.write("\n")


f.write("mv $1.log $1/")
f.truncate()
f.flush()
f.close()

