import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import argparse
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
#https://medium.com/learning-machine-learning/introduction-to-tensorflow-estimators-part-1-39f9eb666bc7

plt.style.use("seaborn-colorblind")
# only displays the most important warnings
tf.logging.set_verbosity(tf.logging.FATAL)
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Building Tensorflow Linear Regression Model", formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-input", "--input", type=str, help="Training Dataset")
    args=parser.parse_args()
    inp_f = args.input
    regression_df = pd.read_csv(inp_f)
    regression_df = regression_df.drop(['end '], axis=1)
    header = regression_df.columns.values.tolist()
    features_header = header[1:]
    target_header   = header[0]
    df = regression_df
    for col in features_header:
        df = df[df[col]==0]
    
    print("%s %d/%d are NULL!" %(inp_f,df.shape[0],regression_df.shape[0]))
    quit()
    regression_df=(regression_df-regression_df.mean())/regression_df.std()
    print(regression_df)
    #scaler = MinMaxScaler() 
    #scaled = scaler.fit_transform(regression_df) 
    #print(type(scaled))
    #regression_dfdf.loc[:,:] = scaled
    print("Number of Training Instances Is %d" %(regression_df.shape[0]))
    print("Regression Fields")
    features = regression_df[features_header]
    target = regression_df[target_header]
    X_train, X_test, y_train, y_test = train_test_split(features, target, test_size=0.33, random_state=42)
    
    feature_columns = [tf.feature_column.numeric_column(key = column) for column in features_header]
    target_column = tf.feature_column.numeric_column(key = target_header)

    training_input_fn = tf.estimator.inputs.pandas_input_fn(x = X_train,y=y_train,batch_size=32,shuffle= True,num_epochs = None)

    eval_input_fn = tf.estimator.inputs.pandas_input_fn(x=X_test,y=y_test,batch_size=32,shuffle=False,num_epochs = 1)
    linear_regressor = tf.estimator.LinearRegressor(feature_columns=feature_columns,model_dir = "%s_model" %(inp_f))
    linear_regressor.train(input_fn=training_input_fn,steps=2000)
    eval_result=linear_regressor.evaluate(input_fn = eval_input_fn)
    output_f="%s_result" %(inp_f)
    print(eval_result)

