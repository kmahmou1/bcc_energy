input=$1
echo "" > report.csv
while IFS= read -r var
do
	pid=$(echo "$var" | cut -d';' -f 1)
	line=$(tail -1 summary_${pid})	
	echo $line >> report.csv
done < "$input"

echo "Calculating Coefficient Done ... Thanks for your cooperation"
