import argparse
import numpy as np
import pandas as pd

parser = argparse.ArgumentParser(description="Tracing Different Java DTrace Probes", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-p", "--pid", type=int, help="The PID of the traced process")
parser.add_argument("-epochs","--epochs", type=int,help="The number of epochs", default=0)
parser.add_argument("-probe","--probe",type=str,default="None")
parser.add_argument("-lib","--lib",type=str,default="None")
args = parser.parse_args()
this_id = int(args.pid)
epochs = args.epochs
unique={}
probe_f = "%d_probes" %(this_id)
energy_f = "energy_%d.data" %(this_id)
probe_name=args.probe
lib_name=args.lib


print("Process ID %d" %(this_id))
print("Reading %s and %s ..." %(probe_f,energy_f))
print("Number of epochs %d" %(epochs))
total_sum=0
energy_data = pd.read_csv(energy_f,names=["cconstant","energy_timestamp","counter_name","counter","pkg","dram","end"])
energy_data = energy_data.drop(['cconstant','counter_name','end'],axis=1)
probe_data = pd.read_csv(probe_f,names=["probe_name","probe_timestamp"])
energy_data_cpy = energy_data.diff().fillna(0)
energy_data_cpy = energy_data_cpy.shift(-1)
energy_data["pkg"]=energy_data_cpy["pkg"]
energy_data["dram"]=energy_data_cpy["dram"]
print("Total Number of Intervals %d" %(energy_data.shape[0]))
print("Total Number of Probes %d" %(probe_data.shape[0]))
no_steps=0
probe_count=[]
for i in range(energy_data.shape[0]):
    probe_count.append(dict())

found=0
interval_distribution=0
interval_no = energy_data.shape[0];
found_intervals=0
print(probe_data.dtypes)
probe_data.sort_values(by=['probe_timestamp'],inplace=True)
energy_data.sort_values(by=['energy_timestamp'],inplace=True)
interval_indx=0
probe_data = probe_data.reset_index()
#quit()
i=0
for i,probe_ts in enumerate(probe_data["probe_timestamp"]):
    probe_name = probe_data["probe_name"][i]
    unique[probe_name]=1
    while (interval_indx < interval_no-1):
        #print("Checking %d agains %d-%d" %(probe_ts,energy_data["energy_timestamp"][interval_indx],energy_data["energy_timestamp"][interval_indx+1]))
        if(probe_ts >= energy_data["energy_timestamp"][interval_indx]  and probe_ts < energy_data["energy_timestamp"][interval_indx+1]):
            
            mp = probe_count[interval_indx]
            if not(probe_name in  mp):
                mp[probe_name]=0
            
               
            mp[probe_name]=mp[probe_name]+1
            no_steps = no_steps+1
            found_intervals=found_intervals+1
            break
        else:  
            total_sum = total_sum + sum(probe_count[interval_indx].values())
            no_steps=no_steps+1
            interval_distribution=interval_distribution+1
            interval_indx=interval_indx+1


    if(interval_indx==interval_no):
        probe_count[interval_no-1]=probe_count[interval_no-1]
        no_steps=no_steps+1
        found_intervals=found_intervals+1

print("Step Count %d \n" %(no_steps))
print("Found Intervals %d \n" %(found_intervals))
print("Number of Probes %d" %(len(unique)))

data_out=[]
for i,mp in enumerate(probe_count):
    energy_value = energy_data["pkg"][i]
    probes_str = str(energy_value) + ","
    probe_counts = probe_count[i]
    for pname in unique:
        if(pname in probe_counts):
            probes_str = probes_str + str(probe_counts[pname])  + ","
        else:
            probes_str = probes_str + "0,"

    probes_str = probes_str + "end"            
    data_out.append(probes_str)
print("Found Intervals %d \n", found_intervals)
print(len(data_out))
f = open("regression_%d" %(this_id), "a")
f.write("energy,")
for u in unique:
    f.write("%s," %(u))
f.write("end \n")
for line in data_out:
    f.write("%s \n" %(line))

f.close()
print("\n Total Sum %d \n" %(total_sum))
